import React from 'react';
import ReactDOM from 'react-dom';
import Main from './containers/Main';
import 'typeface-roboto';
import Base from '@material-ui/core/CssBaseline';

const App = (
    <React.Fragment>
        <Base />
        <Main />
    </React.Fragment>
);

ReactDOM.render(App, document.getElementById('root'));
