const format = params => {
    let q = '';
    for (let [key, value] of Object.entries(params)) {
        if (value) {
            q += `${key}=${encodeURIComponent(value)}&`;
        }
    }
    return q.length > 0 ? q.slice(0, -1) : q;
};

export default { format };
