import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const Container = withStyles(theme => {
    return {
        root: {
            minWidth: 100,
        },
    };
})(Box);

export { Container };
