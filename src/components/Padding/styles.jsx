import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const Container = styled(Box)(({ paddings }) => {
    const { l, r, t, b } = paddings;
    return {
        padding: `${t}px ${r}px ${b}px ${l}px`,
    };
});

export { Container };
