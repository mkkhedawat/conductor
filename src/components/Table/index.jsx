import React from 'react';
import { CustomTable } from './styles';

const Table = props => {
    return (
        <CustomTable
            {...props}
            options={{
                headerStyle: {
                    backgroundColor: '#000',
                    color: '#FFF',
                },
                rowStyle: rowData => ({
                    backgroundColor: rowData.tableData.id % 2 === 0 ? '#eee' : '#fff',
                }),
            }}
        ></CustomTable>
    );
};

export default Table;
