import { withStyles } from '@material-ui/core/styles';
import MaterialTable from 'material-table';

const CustomTable = withStyles(theme => {
    console.log(theme);

    return {
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
    };
})(MaterialTable);

export { CustomTable };
