import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { Dialog, DialogActions, DialogContent, DialogTitle, CloseButton } from './styles';

const CustomDialogTitle = props => {
    const { children, onClose, ...other } = props;
    return (
        <DialogTitle disableTypography {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <CloseButton aria-label="close" onClick={onClose}>
                    <CloseIcon />
                </CloseButton>
            ) : null}
        </DialogTitle>
    );
};

const CustomDialog = ({ children, isOpen = false, onClose, title = 'Details' }) => {
    const [open, setOpen] = useState(isOpen);

    useEffect(() => {
        setOpen(isOpen);
    }, [isOpen]);

    return (
        <Dialog
            onClose={onClose}
            aria-labelledby="customized-dialog-title"
            open={open}
            fullWidth
            maxWidth={'lg'}
        >
            <CustomDialogTitle id="customized-dialog-title" onClose={onClose}>
                {title}
            </CustomDialogTitle>
            <DialogContent dividers>{children}</DialogContent>
            {/* <DialogActions>
                    <Button autoFocus onClick={onClose} color="primary">
                        Save changes
                    </Button>
               </DialogActions>*/}
        </Dialog>
    );
};

export default CustomDialog;
