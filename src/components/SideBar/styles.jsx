import { styled } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import MuiList from '@material-ui/core/List';

const Container = styled(Box)({
    display: 'flex',
    position: 'relative',
    top: 200,
});

const HeaderIconContainer = withStyles(theme => ({
    root: {
        opacity: 0.7,
        marginRight: theme.spacing(4),
    },
}))(Box);

const Content = styled(Box)({
    background: '#fff',
    minHeight: 'calc(100vh - 200px)',
    flex: 1,
});

const List = styled(MuiList)({
    width: '100%',
});

const SideDrawer = withStyles(theme => ({
    root: {
        width: 64,
        overflow: 'hidden',
        transition: 'width 500ms',
        whiteSpace: 'nowrap',
        [theme.breakpoints.up('lg')]: {
            width: 250,
        },
        '&:hover': {
            width: 250,
        },
    },
}))(Box);

const ExpansionPanel = withStyles({
    root: {
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 'auto',
        },
    },
    expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
    root: {
        backgroundColor: 'rgba(0, 0, 0, .03)',
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        marginBottom: -1,
        minHeight: 56,
        padding: '0 16px',
        justifyContent: 'flex-start',
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        paddingLeft: 0,
        paddingRight: 0,
        width: '100%',
    },
}))(MuiExpansionPanelDetails);

export {
    Container,
    SideDrawer,
    Content,
    List,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    HeaderIconContainer,
};
