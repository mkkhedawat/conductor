import React from 'react';
import { Container } from './styles';


const Margin = ({ l, r, t, b, hz = 20, vt, children }) => (
    <Container
        margins={{
            l: l || hz || 0,
            r: r || hz || 0,
            t: t || vt || 0,
            b: b || vt || 0,
        }}
    >
        {children}
    </Container>
);

export default Margin;
