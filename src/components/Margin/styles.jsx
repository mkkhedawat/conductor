import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';


const Container = styled(Box)(({ margins }) => {
    const { l, r, t, b } = margins;
    return {
        margin: `${t}px ${r}px ${b}px ${l}px`,
    };
});

export { Container };
