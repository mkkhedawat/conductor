const updateUser = payload => {
    return {
        type: 'UPDATE_USER',
        payload,
    };
};

export { updateUser };
