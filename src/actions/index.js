import { updateUser } from './user';
import { updateResponse } from './response';

export { updateUser, updateResponse };
