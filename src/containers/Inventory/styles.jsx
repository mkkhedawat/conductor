import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import MuiSelect from '@material-ui/core/Select';

import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';

const CloseButton = withStyles(theme => ({
    root: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
}))(IconButton);

const ActionButtonContainer = withStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
}))(Box);

const DialogContainer = withStyles(theme => ({
    root: {
        //minWidth: '50vw',
        // minHeight: '50vh',
        padding: theme.spacing(2),
    },
}))(Box);

const SearchGrid = withStyles(theme => ({
    root: {
        '& > div': {
            marginTop: theme.spacing(2),
        },
    },
}))(Grid);

const Container = styled(Box)({
    textAlign: 'center',
    position: 'absolute',
    paddingBottom: 15,
    width: '100%',
});

const SearchContainer = styled(Paper)({
    width: '100%',
    padding: '20px 30px',
});

const ResultContainer = styled(Box)({
    width: '100%',
    marginTop: 50,
});

const RowContainer = styled(Box)({
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'auto',
});

const Select = styled(MuiSelect)(({ minWidth }) => ({
    minWidth,
}));

export {
    Container,
    SearchContainer,
    Select,
    ResultContainer,
    DialogContainer,
    CloseButton,
    RowContainer,
    SearchGrid,
    ActionButtonContainer,
};
