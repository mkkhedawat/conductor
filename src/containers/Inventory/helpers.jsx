import React, { useState } from 'react';
import { RowContainer, DialogContainer, Select, ActionButtonContainer } from './styles';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns'; // choose your lib
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import Paper from '@material-ui/core/Paper';
import SideBar from '../../components/SideBar';
import Padding from '../../components/Padding';
import Margin from '../../components/Margin';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Dialog from '../../components/Dialog';
import Table from '../../components/Table';
import GroupButton from '../../components/GroupButton';
import Button from '@material-ui/core/Button';
import SubmitIcon from '@material-ui/icons/Send';
import isEmpty from 'lodash.isempty';
import { fetchTaskData, fetchTaskDialogData } from './api';

const keyValueRow = (key, value) => (
    <Grid item xs={6} style={{ display: 'flex' }}>
        <Typography style={{ fontWeight: 'bold' }} variant="body2">
            {key}
        </Typography>
        <Margin hz={20} vt={0}>
            <Typography variant="body2">{value}</Typography>
        </Margin>
    </Grid>
);

const parseTasksDialogDetailsData = async data => {
    const dData = await fetchTaskDialogData(data);
    return (
        <DialogContainer>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography>Txn master Details - ({dData.id})</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails container>
                    <Grid container spacing={3}>
                        {[
                            keyValueRow('unique seq id', dData.id),
                            keyValueRow('txn id', dData.txnID),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                        ]}
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography>Txn master Details</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails container>
                    <Grid container spacing={3}>
                        {[
                            keyValueRow('unique seq id', '1234'),
                            keyValueRow('txn id', '1234'),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                        ]}
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography>Txn master Details</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails container>
                    <Grid container spacing={3}>
                        {[
                            keyValueRow('unique seq id', '1234'),
                            keyValueRow('txn id', '1234'),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                            keyValueRow('group id', '1234'),
                        ]}
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </DialogContainer>
    );
};

const parseItemsActionDetailsData = (data, handleSubmit) => {
    return (
        <DialogContainer>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container justify="space-between">
                    <TextField label="Comments" />
                    <FormControl>
                        <InputLabel id="inventory-type">Status</InputLabel>
                        <Select labelId="inventory-type" minWidth={150}>
                            <MenuItem value={10}>in progrss</MenuItem>
                            <MenuItem value={20}>done</MenuItem>
                            <MenuItem value={30}>backlog</MenuItem>
                        </Select>
                    </FormControl>
                    <Button
                        variant="contained"
                        color="primary"
                        startIcon={<SubmitIcon />}
                        onClick={handleSubmit}
                    >
                        Done
                    </Button>
                </Grid>
            </MuiPickersUtilsProvider>
            <ActionButtonContainer></ActionButtonContainer>
        </DialogContainer>
    );
};

const formatTasksTableData = async options => {
    const { handleLinkClick, params } = options;
    const rawData = await fetchTaskData(params);
    console.log(rawData);
    const data = rawData.map(d => ({
        id: (
            <Link
                href="#"
                onClick={handleLinkClick.bind(null, 'tasks', {
                    id: d.id,
                })}
            >
                {d.id}
            </Link>
        ),
        fname: 'Mr.',
        lname: 'xyz',
        clientID: 123,
    }));
    return data;
};

export { parseTasksDialogDetailsData, parseItemsActionDetailsData, formatTasksTableData };
