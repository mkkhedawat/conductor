import React, { useState, useEffect } from 'react';
import {
    Container,
    SearchContainer,
    Select,
    ResultContainer,
    DialogContainer,
    ActionButtonContainer,
    SearchGrid,
} from './styles';
import {
    parseTasksDialogDetailsData,
    parseItemsActionDetailsData,
    formatTasksTableData,
} from './helpers';
import Grid from '@material-ui/core/Grid';
import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns'; // choose your lib
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import SideBar from '../../components/SideBar';
import Padding from '../../components/Padding';
import Margin from '../../components/Margin';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Dialog from '../../components/Dialog';
import Table from '../../components/Table';
import GroupButton from '../../components/GroupButton';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import moment from 'moment';

const Inventory = ({ text }) => {
    const [selectedDate, handleDateChange] = useState(new Date());
    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [dialogData, setDialogData] = useState(<div />);
    const [isFirstRun, setIsFirstRun] = useState(true);
    const [exceptionType, setExceptionType] = useState('30');
    const [inventorType, setInventorType] = useState('20');
    const [clientID, setClientID] = useState('');
    const [groupID, setGroupID] = useState('12');
    const [exceptionID, setExceptionID] = useState('');
    const [tasksTableData, setTasksTableData] = useState([]);

    const handleLinkClick = async (type, data) => {
        console.log(type, data);
        const dData = await parseTasksDialogDetailsData(data);
        setDialogData(dData);
        setIsDialogOpen(true);
    };

    const handleItemsButtonClick = (type, data) => {
        console.log(type, data);
        setDialogData(
            parseItemsActionDetailsData(data, () => {
                setIsDialogOpen(false);
            })
        );
        setIsDialogOpen(true);
    };

    const handleSearchSubmit = async () => {
        setTasksTableData([]);
        const data = await formatTasksTableData({
            params: {
                selectedDate: moment(selectedDate).format('DD-MM-YYYY'),
                clientID,
                groupID,
                exceptionID,
                exceptionType,
                inventorType,
            },
            handleLinkClick,
        });
        setTasksTableData(data);
    };

    useEffect(() => {
        if (isFirstRun) {
            setIsFirstRun(false);
            handleSearchSubmit();
        }
    }, []);

    return (
        <Container>
            <SideBar>
                <Margin hz={30} vt={20}>
                    <SearchContainer elevation={1} square>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <SearchGrid container justify="space-between">
                                <KeyboardDatePicker
                                    clearable
                                    label="From Date"
                                    value={selectedDate}
                                    placeholder="10/10/2018"
                                    onChange={date => handleDateChange(date)}
                                    format="MM/dd/yyyy"
                                />
                                <KeyboardDatePicker
                                    clearable
                                    label="To Date"
                                    value={selectedDate}
                                    placeholder="10/10/2018"
                                    onChange={date => handleDateChange(date)}
                                    format="MM/dd/yyyy"
                                />
                                <FormControl>
                                    <InputLabel id="exception-type">Exception Type</InputLabel>
                                    <Select
                                        labelId="exception-type"
                                        minWidth={150}
                                        value={exceptionType}
                                        onChange={e => setExceptionType(e.target.value)}
                                    >
                                        <MenuItem value="" disabled>
                                            Exception Type
                                        </MenuItem>
                                        <MenuItem value={'20'}>Type 1</MenuItem>
                                        <MenuItem value={'20'}>Type 2</MenuItem>
                                        <MenuItem value={'30'}>Type 3</MenuItem>
                                    </Select>
                                </FormControl>
                                <TextField
                                    label="Client ID"
                                    value={clientID}
                                    onChange={e => setClientID(e.target.value)}
                                />
                                <TextField
                                    label="Group ID"
                                    value={groupID}
                                    onChange={e => setGroupID(e.target.value)}
                                />
                                <TextField
                                    label="Exception ID"
                                    value={exceptionID}
                                    onChange={e => setExceptionID(e.target.value)}
                                />
                                <FormControl>
                                    <InputLabel id="inventory-type">Inventor Type</InputLabel>
                                    <Select
                                        labelId="inventory-type"
                                        minWidth={150}
                                        value={inventorType}
                                        onChange={e => setInventorType(e.target.value)}
                                    >
                                        <MenuItem value={'10'}>Type 1</MenuItem>
                                        <MenuItem value={'20'}>Type 2</MenuItem>
                                        <MenuItem value={'30'}>Type 3</MenuItem>
                                    </Select>
                                </FormControl>
                            </SearchGrid>
                            <Margin hz={10} t={40}>
                                <ExpansionPanel square>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography>Advanced Search</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        <Grid container justify="space-between">
                                            <TextField label="Exception ID" />
                                            <KeyboardDatePicker
                                                disableFuture
                                                openTo="year"
                                                format="dd/MM/yyyy"
                                                label="Date of birth"
                                                views={['year', 'month', 'date']}
                                                value={selectedDate}
                                                onChange={handleDateChange}
                                            />
                                            <FormControl>
                                                <InputLabel id="inventory-type">
                                                    Inventor Type
                                                </InputLabel>
                                                <Select labelId="inventory-type" minWidth={150}>
                                                    <MenuItem value={10}>Type 1</MenuItem>
                                                    <MenuItem value={20}>Type 2</MenuItem>
                                                    <MenuItem value={30}>Type 3</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </Margin>
                        </MuiPickersUtilsProvider>
                        <ActionButtonContainer>
                            <GroupButton
                                options={['Submit', 'Reset']}
                                onClick={(key, index) => {
                                    switch (key) {
                                        case 'Submit':
                                            handleSearchSubmit();
                                            break;
                                        case 'Reset':
                                            break;
                                        default:
                                    }
                                }}
                            />
                        </ActionButtonContainer>
                    </SearchContainer>
                    <ResultContainer elevation={0} square>
                        <Table
                            options={{
                                exportButton: true,
                            }}
                            data={tasksTableData}
                            columns={[
                                { title: 'Request Identifier', field: 'id' },
                                { title: 'First Name', field: 'fname' },
                                { title: 'Last Name', field: 'lname', type: 'numeric' },
                                { title: 'Client ID', field: 'clientID', type: 'numeric' },
                            ]}
                            title="Search Result"
                        />
                        <Margin hz={0} vt={20}>
                            <Table
                                options={{
                                    exportButton: true,
                                }}
                                columns={[
                                    { title: 'Request Identifier', field: 'id' },
                                    { title: 'First Name', field: 'fname' },
                                    { title: 'Last Name', field: 'lname', type: 'numeric' },
                                    { title: 'action', field: 'action' },
                                ]}
                                data={[
                                    {
                                        id: (
                                            <Link
                                                href="#"
                                                onClick={handleLinkClick.bind(this, 'tasks', {
                                                    id: 1234,
                                                })}
                                            >
                                                1234
                                            </Link>
                                        ),
                                        fname: 'Mr.',
                                        lname: 'xyz',
                                        action: (
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                startIcon={<EditIcon />}
                                                onClick={handleItemsButtonClick.bind(this, 1234)}
                                            >
                                                Edit
                                            </Button>
                                        ),
                                    },
                                    {
                                        id: (
                                            <Link
                                                href="#"
                                                onClick={handleLinkClick.bind(this, 'tasks', {
                                                    id: 3456,
                                                })}
                                            >
                                                3456
                                            </Link>
                                        ),
                                        fname: 'Mr.',
                                        lname: 'xyz',
                                        clientID: 123,
                                    },
                                ]}
                                title="Next Items"
                            />
                        </Margin>
                    </ResultContainer>
                </Margin>
                <Dialog
                    isOpen={isDialogOpen}
                    onClose={() => {
                        setIsDialogOpen(false);
                    }}
                    children={dialogData}
                />
            </SideBar>
        </Container>
    );
};

export default Inventory;
