import axios from 'axios';
import query from '../../utils/query';

const dummyTasksData = [
    {
        id: 3456,
        fname: 'Mr.',
        lname: 'xyz',
        clientID: 123,
    },
    {
        id: '1234',
        fname: 'Mr.',
        lname: 'xyz1',
        clientID: 13,
    },
    {
        id: '123',
        fname: 'Mr.',
        lname: 'xyz2',
        clientID: 23,
    },
    {
        id: '123',
        fname: 'Mr.',
        lname: 'xyz2',
        clientID: 3,
    },
];

const dummyTaskDialogData = {
    id: 1234,
    txnID: 3245,
};

const fetchTaskData = async params => {
    try {
        console.log(params);
        const q = query.format(params);
        const { data } = await axios.post(`someEndpoint?${q}`, {});
        console.log(data);
    } catch (e) {
        console.error(e);
        return dummyTasksData;
    }
    return [];
};

const fetchTaskDialogData = async params => {
    try {
        console.log(params);
        const q = query.format(params);
        const { data } = await axios.post(`someEndpoint2?${q}`, {});
        console.log(data);
    } catch (e) {
        console.error(e);
        return dummyTaskDialogData;
    }
    return {};
};

export { fetchTaskData, fetchTaskDialogData };
