import React from 'react';
import { MemoryRouter, Switch, Route } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { Background } from './styles';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from '../../reducers/rootReducer';
import theme from '../../theme';
import Inventory from '../Inventory';
import Header from '../Header';

const store = createStore(rootReducer);

const Main = () => {
    return (
        <ThemeProvider theme={theme}>
            <Provider store={store}>
                <Background className="app-root">
                    <MemoryRouter initialEntries={['/']} initialIndex={0}>
                        <Header />
                        <Switch>
                            <Route path={'/'} component={Inventory} />
                        </Switch>
                    </MemoryRouter>
                </Background>
            </Provider>
        </ThemeProvider>
    );
};

export default Main;
