import { styled, withTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const LeftContainer = styled(withTheme(Box))(({ theme }) => ({
    margin: 0,
    padding: 0,
    height: '100%',
    flex: 0,
    [theme.breakpoints.up('sm')]: {
        flex: 'auto',
    },
    [theme.breakpoints.up('md')]: {},
    [theme.breakpoints.up('lg')]: {},
}));

const RightContainer = styled(withTheme(Box))(({ theme }) => ({
    margin: 0,
    padding: 25,
    height: '100%',
    backgroundColor: '#f6f6f6',
    flex: 1,
    [theme.breakpoints.up('sm')]: {
        flexBasis: 475,
        maxWidth: 475,
        padding: '35px 10px',
    },
    [theme.breakpoints.up('md')]: {
        padding: '55px 20px 10px',
    },
    [theme.breakpoints.up('lg')]: {},
}));

const Background = styled(Box)({
    margin: 0,
    padding: 0,
    // width: '100vw',
    height: '100vh',
    overflow: 'hidden',
    display: 'flex',
    backgroundPosition: 'left top',
    backgroundSize: 'cover',
    textAlign: 'center',
});

export { Background, LeftContainer, RightContainer };
