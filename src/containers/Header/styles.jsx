import { styled, withTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';

const SubText = styled(Typography)({
    marginTop: -2,
    fontWeight: 300,
    padding: 0,
});

const UserText = styled(Typography)({
    padding: '0 5px',
});

const UserAvatar = styled(Avatar)({
    height: '3em',
    width: '3em',
});

const PrimaryAppBar = styled(AppBar)({
    minHeight: 100,
    maxHeight: 200,
});

const MenuTab = withStyles(theme => ({
    root: {
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 90,
        background: theme.palette.primary.main,
    },
    label: {},
    selected: {
        background: `linear-gradient(165deg, transparent 29%, ${theme.palette.primary.contrastText} 30%)`,
    },
}))(Tab);

// const UserAvatar = styled(withTheme(Tab))(({ theme }) => {
//     console.log(theme);
//     return {};
// });

export { SubText, UserText, UserAvatar, PrimaryAppBar, MenuTab };
