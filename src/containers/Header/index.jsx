import React from 'react';
import Grid from '@material-ui/core/Grid';
import HelpIcon from '@material-ui/icons/Help';
import IconButton from '@material-ui/core/IconButton';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import { SubText, UserText, UserAvatar, PrimaryAppBar, MenuTab } from './styles';

const Header = () => {
    return (
        <React.Fragment>
            <PrimaryAppBar color="primary" position="sticky" elevation={0}>
                <Toolbar alignItems="start">
                    <Grid item>
                        <UserText variant="h3">Some Logo Here </UserText>
                    </Grid>
                    <Grid container spacing={1} alignItems="center">
                        {/* <Hidden smUp>
                            <Grid item>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    // onClick={onDrawerToggle}
                                    // className={classes.menuButton}
                                >
                                    <MenuIcon />
                                </IconButton>
                            </Grid>
                       </Hidden> */}
                        <Grid item xs />
                        <Grid item>
                            <UserText variant="body1">Conductor Guy</UserText>
                            <SubText variant="body2">Service Advocate</SubText>
                            <SubText variant="body2">Thu, August 8, 2008</SubText>
                        </Grid>
                        <Grid item>
                            <IconButton color="inherit">
                                <UserAvatar src="/static/images/avatar/1.jpg" alt="My Avatar" />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Toolbar>
                <Toolbar>
                    <Grid container alignItems="center" spacing={1}>
                        <Grid item xs>
                            {/* <Typography variant="h5" component="h1">
                                Inventory
                    </Typography> */}
                        </Grid>
                        <Grid item>
                            <Tooltip title="theme">
                                <Switch />
                            </Tooltip>
                        </Grid>
                    </Grid>
                </Toolbar>

                <Tabs value={1} textColor="primary" indicatorColor="white" centered>
                    <MenuTab textColor="inherit" label="Home" />
                    <MenuTab textColor="inherit" label="Inventory" />
                    <MenuTab textColor="inherit" label="Reports" />
                    <MenuTab textColor="inherit" label="Help" />
                </Tabs>
            </PrimaryAppBar>
            
        </React.Fragment>
    );
};

export default Header;
