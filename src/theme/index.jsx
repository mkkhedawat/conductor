import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    // palette: {
    //     text: {
    //         primary: 'rgba(0,0,0,0.7)',
    //     },
    //     primary: {
    //         main: '#c00',
    //     },
    //     secondary: { main: '#c00' },
    // },
    // typography: {
    //     body1: {
    //         color: 'rgba(0,0,0,0.7)',
    //     },
    // },
    // overrides: {
    //     MuiInput: {
    //         root: {
    //             color: 'rgba(0, 0, 0, 0.54) !important',
    //         },
    //         underline: {
    //             '&:after': {
    //                 borderBottom: '1px solid rgba(0,0,0,0.75) !important',
    //             },
    //             '&:before': {
    //                 borderBottom: '1px solid rgba(0,0,0,0.75) !important',
    //             },
    //             '&$disabled:before': {
    //                 borderBottom: `1px dotted rgba(0,0,0,0.5) !important`,
    //             },
    //         },
    //     },
    //     MuiFormLabel: {
    //         root: {
    //             color: 'rgba(0, 0, 0, 0.54) !important',
    //         },
    //     },
    //     MuiRadio: {
    //         root: {
    //             '& .$MuiSvgIcon-root': {
    //                 fontSize: '1rem',
    //             },
    //         },
    //     },
    //     MuiSvgIcon: {
    //         root: { fontSize: '1.1rem' },
    //         fontSizeLarge: {
    //             fontSize: '4rem',
    //         },
    //     },
    //     MuiFormControlLabel: {
    //         label: {
    //             fontSize: '0.9rem',
    //         },
    //     },
    //     MuiCheckbox: {
    //         root: { color: '#c00' },
    //     },
    // },
    // props: {
    //     MuiTextField: {
    //         fullWidth: true,
    //         required: true,
    //     },
    // },
});

export default theme;
